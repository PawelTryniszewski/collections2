package pl.sda.silnia;

import java.math.BigInteger;

public class Silnia {

    private static int cos(int i) {
        return i * (i - 1);
    }

    private static int silnia(int i) {
        if (i < 1)
            return 1;
        else
            return i * silnia(i - 1);
    }

    //Silnia
    private static long silnia1(long i) {
        return i < 1 ? 1 : i * silnia1(i - 1);
    }

    // Potegowanie
    private static double potegowanie(double a, double b) {
        return b == 0 ? 1 : a * potegowanie(a, --b); // robi a*a dopuki b nie rowna sie zero
    }

    //Ciąg fibonacciego
    private static long fibo(long n) {
        return n < 2 ? n : fibo(n - 1) + fibo(n - 2);
    }

    private static long fiboIteracja(long n) {
        long pierwszy = 1;
        long drugi = 1;
        long pomocnicza = 1;
        if (n == 0) {
            return 0;
        }

        for (int i = 2; i < n; i++) {
            pomocnicza = pierwszy + drugi;
            pierwszy = drugi;
            drugi = pomocnicza;
        }

        return pomocnicza;
    }

    //NWD –największy wspólny dzielnik
    // http://eduinf.waw.pl/inf/alg/001_search/0006.php
    // Oblicz największy wspólny dzielnik dwóch liczb implementującalgorytm Euklidesa.
    // a)Metodą iteracyjną,
    // b)Metodą rekurencyjną
    //Euklides wykorzystał prosty fakt, iż NWD liczb a i b dzieli również ich różnicę.
    // Zatem od większej liczby odejmujemy w pętli mniejszą dotąd, aż obie liczby się zrównają.
    // Wynik to NWD dwóch wyjściowych liczb.

    public static int NWD(int a, int b) {
        if (a > b) {
            while (a != b) {
                return NWD(a-b, b);
            }
            return a;

        } else {
            while (a != b) {

               return NWD(a, b-a);

            }
            return b;

        }



    }

    public static void main(String[] args) {
         System.out.println(silnia(5));
        System.out.println(silnia1(14));
        System.out.println(potegowanie(2,6));
        long time = System.currentTimeMillis();
        System.out.println(fiboIteracja(0));
        long time1 = System.currentTimeMillis();
        System.out.println(time1 - time + " milisekund --> Czas Iterracji");
        long time2 = System.currentTimeMillis();
        System.out.println(fibo(0));
        long time3 = System.currentTimeMillis();
        System.out.println(time3 - time2 + " milisekund --> Czas Rekurencji");

        System.out.println(NWD(93 ,19));

    }
}
