package pl.sda.kolejka;

public class Queue {
    private QNote front;
    private QNote rear;


    public Queue() {
        this.front = null;
        this.rear = null;
    }

    public void enqueue(Object o) {
        QNote tmp = new QNote(o);
        if (this.rear == null) {
            this.front = tmp;
            this.rear = tmp;
            return;
        }
        this.rear.setNext(tmp);
        this.rear = tmp;

    }

    public QNote dequeue() {
        if (this.rear == null){
            return null;
        }
        QNote tmp = this.front;
        this.front = this.front.getNext();
        if(this.front == null){
            this.rear = null;
        }
        return tmp;

    }
}
