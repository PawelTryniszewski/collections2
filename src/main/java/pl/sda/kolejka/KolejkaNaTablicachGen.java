package pl.sda.kolejka;

public class KolejkaNaTablicachGen<E> {
    private E[] queue;
    private  int queueSize;
    private int indeks;

    public KolejkaNaTablicachGen(int queueSize) {
        this.queue =(E[]) new Object[queueSize];
        this.queueSize = queueSize;
        this.indeks = -1;
    }

    public void enqueue(E element){
        if (indeks<queueSize){
            queue[indeks++]= element;
            indeks++;

        }else{
            queue = copyQueue();
            enqueue(element);
        }
    }


    private E[] copyQueue() {
        E[] temp = (E[]) new Object[this.queue.length *2];
        queueSize = temp.length;
        for (int i = 0; i < temp.length; i++) {
            temp[i]=queue[i];

        }
        return temp;
    }
}
