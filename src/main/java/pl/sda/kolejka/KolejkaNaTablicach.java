package pl.sda.kolejka;

import pl.sda.Stos.MojWyjatek;

import java.util.Arrays;

public class KolejkaNaTablicach {

    int size;
    Object[] tab;
    int indeks;

    public KolejkaNaTablicach(int size) {
        this.size = size;
        this.tab = new Object[size];
        this.indeks = -1;
    }

    public void setTab(Object[] tab) {
        this.tab = tab;
    }

    @Override
    public String toString() {
        return "KolejkaNaTablicach{" +
                "size=" + size +
                ", tab=" + Arrays.toString(tab) +
                ", indeks=" + indeks +
                '}';
    }

    public KolejkaNaTablicach(int size, Object[] tab, int indeks) {
        this.size = size;
        this.tab = tab;
        this.indeks = indeks;
    }

    public void enqueue(Object o) {
        if (this.indeks < this.tab.length - 1) {
            this.tab[++indeks] = o;
        } else {
            Object[] kopia = new Object[this.tab.length * 2];
            for (int i = 0; i < this.tab.length; i++) {
                kopia[i] = this.tab[i];
            }
            kopia[++indeks] = o;
            this.tab = kopia;

        }
    }
    public boolean offer(Object o){
        if (this.indeks < this.tab.length - 1) {
            this.tab[++indeks] = o;
            return true;
        }
        else return false;

    }

    public Object dequeue() throws MojWyjatek {
        Object tmp = this.tab[0];
        if (this.indeks > 0) {

            for (int i = 0; i < this.tab.length - 1; i++) {
                this.tab[i] = this.tab[i + 1];
            }
            indeks--;
            return tmp;
        }
        if (this.indeks==0){
            this.indeks=-1;
            return this.tab[0];

        }else throw new MojWyjatek("\nKolejka jest pusta\n");
    }
    public boolean poll(){
        Object tmp = this.tab[0];
        if (this.indeks > 0) {

            for (int i = 0; i < this.tab.length - 1; i++) {
                this.tab[i] = this.tab[i + 1];
            }
            indeks--;
            return true;
        }
        if (this.indeks==0){
            this.indeks=-1;
            return true;


        }
        return false;

    }

    public Object peek() throws MojWyjatek {
        if (this.indeks>-1) return this.tab[0];
        else throw new MojWyjatek("\nKolejka jest pusta\n");
    }

    public boolean isEmpty() {
        if (this.indeks == -1) return true;
        else return false;
    }
    //enqueue\offer
    //dequeue\poll
    //peek
    //isEmpty
}
