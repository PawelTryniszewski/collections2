package pl.sda.kolejka;

public class QNote {
    private Object value;
    private QNote next;

    public QNote(Object value) {
        this.value = value;
    }

    public QNote getNext() {
        return next;
    }

    public void setNext(QNote next) {
        this.next = next;
    }

    @Override
    public String toString() {
        return "QNote{" +
                "value=" + value +
                '}';
    }
}
