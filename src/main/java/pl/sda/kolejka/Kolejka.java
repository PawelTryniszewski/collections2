package pl.sda.kolejka;

import pl.sda.Stos.MojWyjatek;

import java.util.Arrays;
import java.util.PriorityQueue;
import java.util.Queue;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

public class Kolejka {
    public static void main(String[] args) {
//        Queue q = new Queue();
//        q.enqueue(10);
//        q.enqueue(20);
//        q.enqueue(30);
//        q.enqueue(40);
//        q.enqueue(50);
//        q.dequeue();
//        q.dequeue();
//        q.dequeue();
//        q.enqueue(60);
//        System.out.println(q.dequeue().toString());
//        System.out.println(q.dequeue().toString());
//        System.out.println(q.dequeue().toString());
//        q.enqueue(12);
//        q.enqueue(32);
//        q.enqueue(144);
//        q.enqueue(121);
//        q.enqueue(144);

        KolejkaNaTablicach kolejka = new KolejkaNaTablicach(2);
        kolejka.enqueue(1);
        kolejka.enqueue(2);
        kolejka.enqueue("Pawel");
        kolejka.enqueue(3);
        kolejka.enqueue("Radek");
        System.out.println(kolejka);
        System.out.println(kolejka.peek());
        System.out.println(kolejka.dequeue());
        System.out.println(kolejka.dequeue());
        System.out.println(kolejka.dequeue());
        kolejka.dequeue();
        System.out.println(kolejka);
        System.out.println(kolejka.peek());
        kolejka.offer("Dawid");
        kolejka.offer("Adam");
        kolejka.enqueue("Bartek");
        System.out.println(kolejka);
        System.out.println(kolejka.isEmpty());
        kolejka.dequeue();
        kolejka.dequeue();
        kolejka.dequeue();
        System.out.println(kolejka.dequeue());
        try {

            kolejka.dequeue();
        } catch (MojWyjatek mojWyjatek) {
            System.out.println(mojWyjatek.getLocalizedMessage());
        }
        System.out.println(kolejka.poll());
        System.out.println(kolejka.offer("jaro"));
        kolejka.poll();
        kolejka.poll();
        kolejka.poll();
        try {

            System.out.println(kolejka.peek());
        }catch (MojWyjatek m){
            System.out.println(m.getLocalizedMessage());
        }
        Queue<Object> queue= new LinkedBlockingQueue<Object>();
        queue.offer("Paweł");
        queue.offer("Daria");
        queue.offer("Radek");
        queue.offer("Adam");
        queue.offer("Bartek");
        queue.offer("Kuba");
        queue.offer("Mateusz");
        System.out.println(queue);
        System.out.println(queue.peek());
        queue.poll();
        System.out.println(queue);


    }
}
