package pl.sda.MapaHash;

import java.util.*;

public class MapaHash<K, V> {
    private List<MyEntry<K, V>> entries;
    private int mapSize = 10000;

    public MapaHash() {
        entries = new ArrayList<MyEntry<K, V>>(mapSize);
        for (int i = 0; i < mapSize; i++) {
            entries.add(null);

        }
    }


    public void put(K key, V value) {
        Pair<K, V> pairy = new Pair<K, V>(key, value);
        int hashCode = key.hashCode() % mapSize;
        MyEntry<K, V> entry = entries.get(hashCode);
        if (entry == null) {
            MyEntry<K, V> newEntry = new MyEntry<K, V>();
            newEntry.add(pairy);
            entries.set(hashCode, newEntry);
        } else if (entry.getPairs().size() == 1) {
            entry.getPairs().get(0).setValue(value);
        } else {
            for (Pair foundP : entry.getPairs()) {
                if (foundP.getKey().equals(pairy.getKey())) {
                    foundP.setValue(pairy.getValue());
                    return;
                }
            }
            entry.add(pairy);
        }
    }

    public void putIfAbsent(K key, V value) {
        if (get(key)==null){
            put(key,value);
        }
        Pair<K, V> pairy = new Pair<K, V>(key, value);
        int hashCode = key.hashCode() % mapSize;
        MyEntry<K, V> entry = entries.get(hashCode);
        Pair<K,V> newPair = new Pair<K, V>(key,value);
        if (entry == null) {
          //  put(newPair.getKey(),newPair.getValue());  // Mniej optynalne rozwiązanie
            MyEntry<K,V> newEntry = new MyEntry<K, V>();
            newEntry.add(newPair);
            entries.set(hashCode,newEntry);
        }else {
            for (Pair<K,V> parki :entry.getPairs()){
                if (parki.getKey().equals(newPair.getKey())){
                    return;
                }
            }
        }
    }

    public MapaHash<K, V> copyInto(MapaHash<K, V> copiedMap) {
        for (MyEntry<K, V> entry : copiedMap.getEntries()) {
            if (entry == null) continue;
            for (Pair<K, V> pair : entry.getPairs()) {
                put(pair.getKey(), pair.getValue());
            }
        }
        return this;
    }

    public void remove(K key) {
        int hashCode = key.hashCode() % mapSize;
        MyEntry<K, V> entry = entries.get(hashCode);
        if (entry != null) {
            for (Pair<K, V> foundP : entry.getPairs()) {
                if (foundP.getKey().equals(key)) {
                    entry.getPairs().remove(foundP);
                    if (entry.getPairs().isEmpty()) {
                        entries.set(hashCode, null);
                    }
                }
            }

        }

    }


    public boolean containsKey(K key) {
        int hashCode = key.hashCode() % mapSize;
        MyEntry<K, V> entry = entries.get(hashCode);
        if (entry != null) {
            if (entry.getPairs().size() == 1) {
                return true;
            } else {
                for (Pair<K, V> foundP : entry.getPairs()) {
                    if (foundP.getKey().equals(key)) {
                        return true;
                    }
                }
            }
        }
        return false;
    }


    public boolean containsValue(V value) {
        if (entries.isEmpty()) {
            return false;
        } else {

            for (MyEntry<K, V> foundP : entries) {
                if (foundP == null) {
                    continue;
                }
                if (foundP.getPairs().get(0).getValue().equals(value)) {
                    if (foundP.getPairs().size() > 1) {
                        for (Pair<K, V> found : foundP.getPairs()) {
                            if (found.getValue().equals(value)) {
                                return true;
                            }
                        }
                    }
                    return true;
                }
            }
            return false;

        }
    }

    public V get(K key) {
        int hashCode = key.hashCode() % mapSize;
        MyEntry<K, V> entry = entries.get(hashCode);
        V value = null;
        if (entry != null) {
            if (entry.getPairs().size() == 1) {
                value = entry.getPairs().get(0).getValue();
            } else {
                for (Pair<K, V> foundP : entry.getPairs()) {
                    if (foundP.getKey().equals(key)) {
                        value = foundP.getValue();
                    }
                }
            }
        }
        return value;
    }

    public List<MyEntry<K, V>> getEntries() {
        return entries;
    }
}
