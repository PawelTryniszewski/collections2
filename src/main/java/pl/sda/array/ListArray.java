package pl.sda.array;

import java.util.Arrays;

public class ListArray {

    private Object[] tab;
    private int size=0;

    @Override
    public String toString() {
        return "ListArray{" +
                "tab=" + Arrays.toString(tab) +
                ", size=" + size +
                '}';
    }
    public Object get(int indeks){
        return this.tab[indeks];
    }
    // indexOf(Object o) - zwraca indeks elementu na którym występuje element o

    public int indeksOf(Object o){

        for (int i = 0; i <this.tab.length ; i++) {
            if (this.tab[i]==o){

            return i;
            }
        }
        return -1;
    }
    // contains (Object o) - zwraca true/false czy lista posiada dany obiekt
    public boolean czyPosiada(Object o){
        for (int i = 0; i <this.tab.length ; i++) {
            if (this.tab[i]==o)
                return true;

        }
        return false;
    }

    public boolean czyPosiadaWersja2(Object o){
        return indeksOf(o)!=0;
    }
    // clear() - czyści tablicę z elementów

    public void clear(){
        this.size=0;
    }

    public void remove(int indeks){
        for (int i = indeks; i <this.tab.length ; i++) {
            this.tab[indeks]=this.tab[indeks+1];
        }
        this.tab[size]=null;
        this.size--;
    }

    //add(Object o) dodanie varrags
    public void addV(Object... o ){
        for (Object object:o) {
            add(o);
        }
    }

//
//    public ListArray() {
//        this.tab = new Object[1000];
//    }
    public ListArray(int indeks) {
        this.tab = new Object[indeks];
    }
    public void add(Object object){
        if (this.size>this.tab.length){
        //wstawiamy do tablicy o indeksie [obecny rozmiar}
            Object[] kopia = new Object[this.tab.length*2];
            for (int i = 0; i <this.tab.length ; i++) {

                kopia[i] = this.tab[i];
            }
        }
            this.tab[size++]= object;


    }
    public int size(){
        for (int i = 0; i <tab.length ; i++) {
            if (tab[i]==null){
                return i;
            }

        }
       return tab.length;
    }
    //set(int indeks, Object o) - ustawia wartość na indeksie
    public void setObj(int indeks, Object o){
        this.tab[indeks]= o;
    }

    // do domu !!!!!!!!!!!!!!!!!!!;
    //remove(Object o) -- usuwa obiekt o z listy  (najpierw znajduje potem przepisuje i usowa ostatni)
    public void remove(Object o){
        int indeks = 0;
        for (int i = 0; i < this.tab.length; i++) {
            if (this.tab[i]==o){
                indeks=i;
            }
        }
        for (int i = indeks; i <this.tab.length ; i++) {

            this.tab[i]=this.tab[i+1];
        }
        size--;
    }
    //isEmpty() - zwraca info czy lista jest pusta (czy size =0)
    public boolean isEmpty(){
        if (size==0){
            return true;
        }else
            return false;
    }
    //removeAll(Object o) - usuwa wszystkie wystąpienia elementu z listy(znajduje indeksy zawieraajace i je wywala)
    public void removeAll(Object o){
        for (int i = 0; i < this.tab.length; i++) {
            if (this.tab[i]==o){
                remove(o);
            }
        }
    }
    //lastIndexOf(Object o) - znajduje ostatni indeks elementu o w liście(zwraca tab[size].
    public Object lastIndeksOf(Object o){
        return this.tab[size];
    }
    //add(int i, Object o) wstaw element o na indeks
    public void add(int indeks, Object object){

        if (this.size>this.tab.length){
            //wstawiamy do tablicy o indeksie [obecny rozmiar}
            Object[] kopia = new Object[this.tab.length*2];
            for (int i = 0; i <this.tab.length ; i++) {

                kopia[i] = this.tab[i];
            }
        }
        this.tab[size++]= object;



        for (int i = indeks; i <this.tab.length ; i++) {
            this.tab[indeks+1]=this.tab[indeks];

        }
        this.tab[indeks]=object;

    }
}
