package pl.sda.sortowania;

import java.util.Arrays;
import java.util.Collections;
import java.util.Random;

public class Main {
    public static void main(String[] args) {


        Random r = new Random();
        int[] tab = new int[33];
        for (int i = 0; i < 33; i++) {
            tab[i] = r.nextInt(20);

        }
        wypisz(tab);
        posortujWstawianie(tab);
        wypisz(tab);
        System.out.println(znajdzIndeks(tab, 12));
//        System.out.println(znajdzIndeksRek(tab, 12, 0));
        System.out.println(znajdzIndeksRek1(tab, 12, 0));
        System.out.println(wyszukiwanieBinarne(tab,12));
        System.out.println(wyszukiwanieBinarneRek(tab,12,0,tab.length));

    }

    public static int wyszukiwanieBinarne(int[] tab,int szukanaLiczba)
    {
        int poczatek = 0, koniec = tab.length - 1, srodek = 0;

        while(poczatek <= koniec)
        {
            srodek = (poczatek + koniec) / 2;
            if(tab[srodek] == szukanaLiczba)
                return srodek;
            else if(tab[srodek] < szukanaLiczba)
                poczatek = srodek + 1;
            else
                koniec = srodek - 1;
        }
        return -1;
    }

    public static int wyszukiwanieBinarneRek(int[] tab,int szukanaLiczba,int poczatek ,int koniec ){
        int srodek =(poczatek + koniec) / 2;

        if (szukanaLiczba==tab[srodek]&&poczatek <= koniec){
            return srodek;
        }
        if (tab[srodek]<szukanaLiczba&&poczatek <= koniec){
            return wyszukiwanieBinarneRek(tab,szukanaLiczba,++srodek,koniec);
        }
        if (tab[srodek]>szukanaLiczba&&poczatek <= koniec){
            return wyszukiwanieBinarneRek(tab,szukanaLiczba,poczatek,--srodek );
        }
        return -1;
    }

    public static int znajdzIndeks(int[] tab, int i) {
        int indeks = 0;
        for (int j = 0; j < tab.length; j++) {

            if (tab[j] == i) {
                indeks = j;
                return indeks;
            }
        }
        return -1;

    }

    public static int znajdzIndeksRek(int[] tab, int i, int j) {
        return i == tab[j]? j : znajdzIndeksRek(tab, i, ++j);
    }

    public static int znajdzIndeksRek1(int[] tab, int i, int j) {
        if (tab[j] == i) return j;
        if (j >= tab.length-1) return -1;
        return znajdzIndeksRek1(tab, i, ++j);
    }

    public static void wypisz(int[] tab) {
        System.out.println(Arrays.toString(tab));
    }

    public static void posortujWybieranie(int[] tab) {
        for (int i = 0; i < tab.length; i++) {

            int min = i;
            for (int j = i + 1; j < tab.length; j++) {
                if (tab[j] < tab[min]) {
                    min = j;
                }

            }
            int tmp = tab[min];

            tab[min] = tab[i];
            tab[i] = tmp;

        }
    }

    public static void posortujWstawianie(int[] tab) {
        int i, j, v;

        for (i = 1; i < tab.length; i++) {
            j = i;
            v = tab[i];
            while ((j > 0) && (tab[j - 1] > v)) {
                tab[j] = tab[j - 1];
                j--;
            }
            tab[j] = v;
        }
    }

    public static void posortujBubble(int[] tab) {
        int zapisany = 0;
        int licznik = 1;

        while (licznik < tab.length - 1) {
            for (int i = 0; i < (tab.length - licznik); i++) {
                if (tab[i] < tab[i + 1]) {
                    zapisany = tab[i + 1];
                    tab[i + 1] = tab[i];
                    tab[i] = zapisany;
                }
            }
            licznik++;
        }
    }


//    private static void quicksort(int tab[], int x, int y) {
//
//        int i,j,v,temp;
//
//        i=x;
//        j=y;
//        v=tab[(x+y) / 2];
//        do {
//            while (tab[i]<v)
//                i++;
//            while (v<tab[j])
//                j--;
//            if (i<=j) {
//                temp=tab[i];
//                tab[i]=tab[j];
//                tab[j]=temp;
//                i++;
//                j--;
//            }
//        }
//        while (i<=j);
//        if (x<j)
//            quicksort(tab,x,j);
//        if (i<y)
//            quicksort(tab,i,y);
//    }

}



