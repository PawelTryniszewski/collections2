package pl.sda.HashSet;

import java.util.List;

public class SetHash<E> {
    private E[] elements;
    private static final int MAX_SIZE = 5000;

    public SetHash(E[] elements) {
        this.elements = (E[]) new Object[MAX_SIZE];
    }

    public void add(E element) {
        int hashCode = Math.abs(element.hashCode() % elements.length);
        if (elements[hashCode] == null) {
            elements[hashCode] = element;
        } else {
            int indeks = hashCode;
            while (elements[hashCode] != null) {
                indeks++;
                if (indeks == elements.length) {
                    indeks = 0;
                } else if (indeks == hashCode) {
                    resizeArray();
                    add(element);
                }
            }
            elements[indeks] = element;

        }
    }

    private void resizeArray() {
        E[] resizedArray = (E[]) new Object[elements.length * 2];
        for (int i = 0; i < elements.length; i++) {
            resizedArray[i] = elements[i];
        }
        elements = resizedArray;

    }

    public boolean remove(E element) {
        int hashCode = Math.abs(element.hashCode() % elements.length);
        int indeks = hashCode;
        if (elements[hashCode]==null) return false;
        else if (elements[hashCode]==element){
            elements[hashCode]=null;
            return true;
        }else {
        while (elements[hashCode] != null) {
            indeks++;
            if (indeks == elements.length) {
                indeks = 0;
            } else if (indeks == hashCode) {
                return false;
            }
        }
        elements[indeks] = null;
        return true;

        }


    }

    public boolean contains(E element) {
        int hashCode = Math.abs(element.hashCode() % elements.length);
        if (elements[hashCode] == null) {
            return false;
        } else if (elements[hashCode] == element) {
            return true;
        } else {
            int index = hashCode;
            while (elements[index] != null) {
                index++;
                if (index == elements.length) {
                    index = 0;
                } else if (index == hashCode) {
                    return false;
                }
            }
            return true;
        }
    }
    
}
