package pl.sda.linked;

public class ListElement {
    private Object value;
    private ListElement next;
    private ListElement previous;

    public ListElement(Object value) {
        this.value = value;
        this.next = null;
        this.previous = null;
    }



    public Object getValue() {
        return value;
    }

    public void setValue(Object value) {
        this.value = value;
    }

    public ListElement getNext() {
        return next;
    }

    public void setNext(ListElement next) {
        this.next = next;
    }

    public ListElement getPrevious() {
        return previous;
    }

    public void setPrevious(ListElement previous) {
        this.previous = previous;
    }
}
