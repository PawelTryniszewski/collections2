package pl.sda.linked;

public class ListLinked {
    private ListElement head;
    private ListElement tail;

    private int size;

    public ListLinked() {
        this.head = null;
        this.tail = null;
        this.size = 0;
    }

    public void removeLast() {

        this.tail = this.tail.getPrevious();
        this.tail.setNext(null);

    }


    public void removeFirst() {
        this.head = this.head.getNext();
        this.head.setPrevious(null);
    }

    // indexOf(Object o) - zwraca indeks elementu na którym występuje element o
    public int indeksOf(Object o) {
        ListElement szukany = this.head;
        int indeks = 0;
        while (szukany.getValue() != o) {
            szukany = szukany.getNext();
            indeks++;
        }
        return indeks;
    }

    // contains (Object o) - zwraca true/false czy lista posiada dany obiekt
    public boolean czyZawiera(Object o) {
        ListElement szukany = this.head;
        while (szukany.getValue() != o) {
            szukany = szukany.getNext();
            if (szukany.getValue() == o) {
                return true;
            }

        }
        return false;
    }

// clear() - czyści tablicę z elementów

    public void clear() {
        this.head = null;
        this.tail = null;
        size = 0;
    }

    // add(Object... o) - dodaje elementy varargs do listy
    public void addV(Object... o) {
        for (Object object : o) {
            add(o);
        }
    }
// get(int index)
    public Object get(int indeks){
        ListElement tpm = this.head;
        Object szukany = null;
        int licznik=0;
        while (tpm != null) {
            tpm = tpm.getNext();
            licznik++;
            if (licznik==indeks){
                szukany=tpm;
            }
        }
        return szukany;
    }


    public void remove(int indeks) {

//        if (indeks == 0) {
//            removeFirst();
//            return;
//        }

        ListElement tpm = this.head;
        int count = 0;
        while (tpm != null && count != 0) {

            tpm = tpm.getNext();
            count++;
        }

        if (tpm == null) {
            throw new ArrayIndexOutOfBoundsException("out of bounds");
        }
        ListElement poprzednik = tpm.getPrevious();
        ListElement nastepnik = tpm.getNext();

        if (poprzednik != null) {
            poprzednik.setNext(nastepnik);
        } else {
            head = head.getNext();
        }
        if (nastepnik != null) {
            nastepnik.setPrevious(poprzednik);
        } else {
            tail = tail.getPrevious();
        }
        this.size--;
    }

    @Override
    public String toString() {
        return "ListLinked{" +
                "data=" + toString1() +
                '}';
    }

    private String toString1() {
        StringBuilder builder = new StringBuilder();
        ListElement tpm = this.head;
        while (tpm != null) {

            builder.append(tpm.getValue() + " ");
            tpm = tpm.getNext();
        }

        return builder.toString();
    }

    //add(Object 0)
    public void add(Object dane) {
        ListElement element = new ListElement(dane);
        if (size == 0 && head == null && tail == null) {
            this.head = element;
            this.tail = element;

        } else if (size == 1 && head == tail) {
            this.tail = element;
            this.head.setNext(element);
            element.setPrevious(this.head);
        } else {
            this.tail.setNext(element);
            element.setPrevious(this.tail);
            this.tail = element;
        }
        this.size++;
    }
}
