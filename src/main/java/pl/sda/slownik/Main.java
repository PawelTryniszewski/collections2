package pl.sda.slownik;

import pl.sda.MapaHash.MapaHash;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Słownik slownik = new Słownik();
        slownik.dodajWpis("Pies", "Dog");
        slownik.dodajWpis("Kot", "Cat");
        slownik.dodajWpis("Lew", "Lion");
        slownik.dodajWpis("Słoń", "Elephant");
        slownik.dodajWpis("Koń", "Horse");
        slownik.dodajWpis("Mysz", "Mouse");
        slownik.dodajWpis("Zebra", "Zebra");

        Scanner sc = new Scanner(System.in);
        String slowo;
        do {
            slowo = sc.nextLine();
            if (!slowo.equals("quit")) {
                System.out.println(slownik.znajdzTlumaczenie(slowo));
            }
        } while (!slowo.equals("quit"));


    }
}
