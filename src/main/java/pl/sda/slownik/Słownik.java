package pl.sda.slownik;

import pl.sda.MapaHash.MapaHash;

import java.util.ArrayList;

public class Słownik extends MapaHash {
    MapaHash<String,String> slownik= new MapaHash<String, String>();

    public Słownik() {
        this.slownik = slownik;
    }

    //dodajWpis –metoda dodając do mapy gdzie kluczem jest słowo polskie, a wartościąsłowo angielskie

    public void dodajWpis(String slowoPolskie, String slowoAngielskie){
        this.slownik.put(slowoPolskie,slowoAngielskie);
    }

    public String znajdzTlumaczenie(String slowo){
        return slownik.get(slowo);

    }
}
