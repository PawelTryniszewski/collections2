package pl.sda.ONP;

import java.util.Stack;

public class ONP {
    public static int solveONP(String[] chars) {
        Stack<Integer> stack = new Stack();
        for(int i = 0; i < chars.length; i++) {
            if(chars[i].equals("+")) {
                int a = stack.pop();
                int b = stack.pop();
                a = a + b;
                stack.push(a);
            }
            else if(chars[i].equals("*")) {
                int a = stack.pop();
                int b = stack.pop();
                a = a * b;
                stack.push(a);
            }else if(chars[i].equals("-")) {
                int a = stack.pop();
                int b = stack.pop();
                a = b-a;
                stack.push(a);
            }else if(chars[i].equals("/")) {
                int a = stack.pop();
                int b = stack.pop();
                a = b/a;
                stack.push(a);
            }
            else {
                int a = Integer.parseInt(chars[i]);
                stack.push(a);
            }
        }
        return stack.pop();
    }

}
