package pl.sda.Stos;

import java.util.Arrays;

public class Stos<T> {
    int size;
    T[] tab;
    int top;

    @Override
    public String toString() {
        return "Stos{" +
                "size=" + size +
                ", tab=" + Arrays.toString(tab) +
                ", top=" + top +
                '}';
    }

    public Stos(int maxRozmiar) {
        this.size = maxRozmiar;
        this.tab = (T[]) new Object[size];
        this.top = -1;
    }

    public int getSize() {
        return size;
    }

    public Object[] getTab() {
        return tab;
    }

    public int getTop() {
        return top;
    }

    public void push(T o) {

        if (this.top < this.tab.length - 1) {
             this.tab[++top] = o;
        } else {
            T[] kopia = (T[]) new Object[tab.length * 2];
            for (int i = 0; i < tab.length; i++) {
                kopia[i] = this.tab[i];
            }
            kopia[++top] = o;
            this.tab = (T[]) kopia;
        }
    }

    public Object pop() throws MojWyjatek {
        if (top>=0) {
            return tab[top--];
        }
        else
            throw new MojWyjatek("Nie ma wiecej obiektow do zdjęcia");
    }

    public Object peek() throws MojWyjatek {
        if (top == -1) {
            throw new MojWyjatek("Lista jest pusta");
        }
        return tab[top];
    }


    public boolean isEmpty() throws MojWyjatek {
        if (this.top < 0) {
            return true;
        } else throw new MojWyjatek("Nie jest pusty");
    }

    public int size() {
        return top + 1;
    }

    public boolean contains(Object o) {
        for (int i = 0; i < tab.length; i++) {
            if (tab[i].equals(o)) {
                return true;
            }
        }
        return false;
    }

    public void clear() {
        for (int i = 0; i < this.tab.length; i++) {
            tab[i] = null;
        }
    }
}
