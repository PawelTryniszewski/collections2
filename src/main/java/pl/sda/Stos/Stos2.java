package pl.sda.Stos;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Stos2 <T>{
    int size;
    List list;
    int top;


    public Stos2() {
        this.list = new ArrayList<T>();
        this.top = -1;
    }

    public int getSize() {
        return size;
    }


    public int getTop() {
        return top;
    }

    public void push(T o) {

            this.list.add(++top,o);

    }

    public Object pop() throws MojWyjatek {
        if (top>=0) {
            return list.get(top--);
        }
        else
            throw new MojWyjatek("Nie ma wiecej obiektow do zdjęcia");
    }

    public Object peek() throws MojWyjatek {
        if (top == -1) {
            throw new MojWyjatek("Lista jest pusta");
        }
        return list.get(top);
    }


    public boolean isEmpty() throws MojWyjatek {
        if (this.top < 0) {
            return true;
        } else throw new MojWyjatek("Nie jest pusty");
    }

    public int size() {
        return top + 1;
    }

    public boolean contains(Object o) {
        for (int i = 0; i < list.size(); i++) {
            if (list.get(i).equals(o)) {
                return true;
            }
        }
        return false;
    }

    public void clear() {
        for (int i = 0; i < this.list.size(); i++) {
            list.add(i,null);
        }
    }
}
